package org.eclipse.springthymeleaf.repository;

import java.util.List;

import org.eclipse.springthymeleaf.model.Personne;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonneRepository extends JpaRepository<Personne, Long> {

	List<Personne> findByNomAndPrenom(String nom, String prenom);

	Personne findByNum(Long num);

}
